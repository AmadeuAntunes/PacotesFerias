﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PacotesFerias
{
    public partial class MenuPrincipal : Form
    {
        CriarPacote win;
        VerPacotes winv;
        EditarPacotes wine;
        ApagarPacotes winap;
        public MenuPrincipal()
        {
             InitializeComponent();
             win = new CriarPacote();
             winv = new VerPacotes();
             wine = new EditarPacotes();
            winap = new ApagarPacotes();
          
        }
    }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnCriarPacote_Click(object sender, EventArgs e)
        {
            Hide();
            win.Show();
        }

        private void btnVerPacotes_Click(object sender, EventArgs e)
        {

            if (Pacote.GetSizeList() > 0)
            {
                Hide();
                winv.Show();
            }
            else
            {
                MessageBox.Show("Não existem pacotes a consultar");
            }
           
        }

        private void btnEditarPacotes_Click(object sender, EventArgs e)
        {
            if(Pacote.GetSizeList() > 0)
            {
                Hide();
                wine.Show();
            }
            else
            {
                MessageBox.Show("Não existem pacotes a editar");
            }
           
        }

        private void btnApagarPacotes_Click(object sender, EventArgs e)
        {


            if (Pacote.GetSizeList() > 0)
            {
                Hide();
                winap.Show();
            }
            else
            {
                MessageBox.Show("Não existem pacotes a apagar");
            }
           
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Amadeu Antunes"
                + "\nVersão 0.1"
                );
        }

        private void guardarComoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Stream myStream;
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                if ((myStream = saveFileDialog.OpenFile()) != null)
                {
                    myStream.Close();
                    string path = Path.GetFullPath(saveFileDialog.FileName);
                    string createText = String.Empty;
                    foreach (Pacote p in Pacote.GetLista())
                    {
                        createText += p.ToString() + Environment.NewLine;
                    }   
                    File.WriteAllText(path, createText);
               
                }
            }
        }

        private void guardarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string createText = string.Empty;
            using (StreamWriter writetext = new StreamWriter("db.txt"))
            {
                foreach (Pacote p in Pacote.GetLista())
                {
                    createText += p.ToString() + Environment.NewLine;
                }

                
                writetext.WriteLine(createText);
            }
        }



        private void abrirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Stream myStream;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                if ((myStream = openFileDialog.OpenFile()) != null)
                {
                    myStream.Close();
                    string path = Path.GetFullPath(openFileDialog.FileName);
                    string createText = String.Empty;
                    string text = File.ReadAllText(path);
                
                    string[] data = text.Split('#');
                    Pacote.Carregar(data);
                }
            }
        }
    }
}
