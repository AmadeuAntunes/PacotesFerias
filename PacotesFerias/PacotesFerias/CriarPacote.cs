﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PacotesFerias
{
    public partial class CriarPacote : Form
    {
        MenuPrincipal win;
        public CriarPacote()
        {
            InitializeComponent();
            TxtIDPacote.Text = (Pacote.getId() + 1).ToString();

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            win = new MenuPrincipal();
            Hide();
            win.Show();
        }

        private void btnCheck_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(TxtDescPacote.Text))
            {
                MessageBox.Show("Introduza a descrição do pacote");
                return;
            }
           

            if (Convert.ToDouble(TxtPreco.Value) <= 0)
            {
                MessageBox.Show("Introduza o preço do pacote");
                return;
            }
            Pacote.AddLista(new Pacote(TxtDescPacote.Text, Convert.ToDouble(TxtPreco.Value)));
            MessageBox.Show("Pacote criado com Sucesso!");
            win = new MenuPrincipal();
            Hide();
            win.Show();
        }
    }
}
