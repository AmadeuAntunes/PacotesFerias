﻿using System;
using System.Windows.Forms;

namespace PacotesFerias
{
    public partial class VerPacotes : Form
    {
        MenuPrincipal win;
        public VerPacotes()
        {
            InitializeComponent();
            foreach (var item in Pacote.GetLista())
            {
                dataGridView1.Rows.Add(item.ID, item.DescricaoDoPacote, item.Preco);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            win = new MenuPrincipal();
            Hide();
            win.Show();
        }

    
    }
}
