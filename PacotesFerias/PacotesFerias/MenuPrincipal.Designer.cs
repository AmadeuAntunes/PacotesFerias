﻿namespace PacotesFerias
{
    partial class MenuPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuPrincipal));
            this.btnClose = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.ficheiroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.guardarComoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.guardarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnApagarPacotes = new System.Windows.Forms.Button();
            this.btnVerPacotes = new System.Windows.Forms.Button();
            this.btnEditarPacotes = new System.Windows.Forms.Button();
            this.btnCriarPacote = new System.Windows.Forms.Button();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.abrirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnClose.BackgroundImage")));
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClose.Location = new System.Drawing.Point(339, 316);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(118, 55);
            this.btnClose.TabIndex = 0;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ficheiroToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(473, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // ficheiroToolStripMenuItem
            // 
            this.ficheiroToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.guardarComoToolStripMenuItem,
            this.guardarToolStripMenuItem,
            this.abrirToolStripMenuItem});
            this.ficheiroToolStripMenuItem.Name = "ficheiroToolStripMenuItem";
            this.ficheiroToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.ficheiroToolStripMenuItem.Text = "Ficheiro";
            // 
            // guardarComoToolStripMenuItem
            // 
            this.guardarComoToolStripMenuItem.Name = "guardarComoToolStripMenuItem";
            this.guardarComoToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.guardarComoToolStripMenuItem.Text = "Guardar Como";
            this.guardarComoToolStripMenuItem.Click += new System.EventHandler(this.guardarComoToolStripMenuItem_Click);
            // 
            // guardarToolStripMenuItem
            // 
            this.guardarToolStripMenuItem.Name = "guardarToolStripMenuItem";
            this.guardarToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.guardarToolStripMenuItem.Text = "Guardar";
            this.guardarToolStripMenuItem.Click += new System.EventHandler(this.guardarToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // btnApagarPacotes
            // 
            this.btnApagarPacotes.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnApagarPacotes.BackgroundImage")));
            this.btnApagarPacotes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnApagarPacotes.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnApagarPacotes.FlatAppearance.BorderSize = 0;
            this.btnApagarPacotes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnApagarPacotes.Location = new System.Drawing.Point(236, 167);
            this.btnApagarPacotes.Name = "btnApagarPacotes";
            this.btnApagarPacotes.Size = new System.Drawing.Size(218, 116);
            this.btnApagarPacotes.TabIndex = 0;
            this.btnApagarPacotes.UseVisualStyleBackColor = true;
            this.btnApagarPacotes.Click += new System.EventHandler(this.btnApagarPacotes_Click);
            // 
            // btnVerPacotes
            // 
            this.btnVerPacotes.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnVerPacotes.BackgroundImage")));
            this.btnVerPacotes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnVerPacotes.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnVerPacotes.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnVerPacotes.FlatAppearance.BorderSize = 0;
            this.btnVerPacotes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVerPacotes.Location = new System.Drawing.Point(236, 52);
            this.btnVerPacotes.Name = "btnVerPacotes";
            this.btnVerPacotes.Size = new System.Drawing.Size(218, 116);
            this.btnVerPacotes.TabIndex = 0;
            this.btnVerPacotes.UseVisualStyleBackColor = true;
            this.btnVerPacotes.Click += new System.EventHandler(this.btnVerPacotes_Click);
            // 
            // btnEditarPacotes
            // 
            this.btnEditarPacotes.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnEditarPacotes.BackgroundImage")));
            this.btnEditarPacotes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnEditarPacotes.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnEditarPacotes.FlatAppearance.BorderSize = 0;
            this.btnEditarPacotes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditarPacotes.Location = new System.Drawing.Point(19, 167);
            this.btnEditarPacotes.Name = "btnEditarPacotes";
            this.btnEditarPacotes.Size = new System.Drawing.Size(218, 116);
            this.btnEditarPacotes.TabIndex = 0;
            this.btnEditarPacotes.UseVisualStyleBackColor = true;
            this.btnEditarPacotes.Click += new System.EventHandler(this.btnEditarPacotes_Click);
            // 
            // btnCriarPacote
            // 
            this.btnCriarPacote.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCriarPacote.BackgroundImage")));
            this.btnCriarPacote.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnCriarPacote.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnCriarPacote.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnCriarPacote.FlatAppearance.BorderSize = 0;
            this.btnCriarPacote.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCriarPacote.Location = new System.Drawing.Point(19, 52);
            this.btnCriarPacote.Name = "btnCriarPacote";
            this.btnCriarPacote.Size = new System.Drawing.Size(218, 116);
            this.btnCriarPacote.TabIndex = 0;
            this.btnCriarPacote.UseVisualStyleBackColor = true;
            this.btnCriarPacote.Click += new System.EventHandler(this.btnCriarPacote_Click);
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "txt";
            this.saveFileDialog.Filter = "Text documents (.txt)|*.txt";
            this.saveFileDialog.Title = "Guardar Como";
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            // 
            // abrirToolStripMenuItem
            // 
            this.abrirToolStripMenuItem.Name = "abrirToolStripMenuItem";
            this.abrirToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.abrirToolStripMenuItem.Text = "Abrir";
            this.abrirToolStripMenuItem.Click += new System.EventHandler(this.abrirToolStripMenuItem_Click);
            // 
            // MenuPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(473, 383);
            this.ControlBox = false;
            this.Controls.Add(this.btnEditarPacotes);
            this.Controls.Add(this.btnCriarPacote);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnVerPacotes);
            this.Controls.Add(this.btnApagarPacotes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MenuPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Button btnApagarPacotes;
        private System.Windows.Forms.Button btnVerPacotes;
        private System.Windows.Forms.Button btnEditarPacotes;
        private System.Windows.Forms.Button btnCriarPacote;
        private System.Windows.Forms.ToolStripMenuItem ficheiroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem guardarComoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem guardarToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.ToolStripMenuItem abrirToolStripMenuItem;
    }
}

