﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PacotesFerias
{
    public partial class EditarPacotes : Form
    {
        MenuPrincipal win;
        public List<Pacote> Pacotes;
        public int N = 0;
        public void atualizar(int n)
        {
            if(n >= 0 && n < Pacotes.Count)
            {
                TxtDescPacote.Text = Pacotes.ElementAt(n).DescricaoDoPacote.ToString();
                TxtPreco.Text = Pacotes.ElementAt(n).Preco.ToString();
                TxtIDPacote.Text = Pacotes.ElementAt(n).ID.ToString();
            }
        }
      

        public EditarPacotes()
        {
            InitializeComponent();
            TxtIDPacote.Text = (Pacote.getId() + 1).ToString();
            Pacotes = Pacote.GetLista();

            try
            {
                atualizar(N);
            }
            catch (Exception)
            {

                
            }
               
        

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            win = new MenuPrincipal();
            Hide();
            win.Show();
        }

        private void btnCheck_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(TxtDescPacote.Text))
            {
                MessageBox.Show("Introduza a descrição do pacote");
                return;
            }
           

            if (Convert.ToDouble(TxtPreco.Value) <= 0)
            {
                MessageBox.Show("Introduza o preço do pacote");
                return;
            }
           
            if(Pacote.EditarLista(new Pacote(TxtDescPacote.Text, Convert.ToDouble(TxtPreco.Value))) == true)
            {
                MessageBox.Show("Pacote substituido com Sucesso!");
            }
            win = new MenuPrincipal();
            Hide();
            win.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (N < Pacotes.Count)
            {
                N++;
                atualizar(N);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (N > 0)
            {
                N--;
                atualizar(N);
            }
              
        }
    }
}
