﻿using System;
using System.Windows.Forms;

namespace PacotesFerias
{
    public partial class ApagarPacotes : Form
    {
        MenuPrincipal win;
        public ApagarPacotes()
        {
            InitializeComponent();
            foreach (var item in Pacote.GetLista())
            {
                ComboBox.Items.AddRange(
                  new object[] { item.ToString() });
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            win = new MenuPrincipal();
            Hide();
            win.Show();
        }

        private void ComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
              
           
        }

        private void ComboBox_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

        private void btnCheck_Click(object sender, EventArgs e)
        {

            if (ComboBox.SelectedItem != null)
            {
                string[] words = ComboBox.SelectedItem.ToString().Split(' ');
                if (Pacote.Apagar(Convert.ToInt32(words[1])) == true)
                {
                    ComboBox.ResetText();
                    ComboBox.SelectedItem = -1;
                    ComboBox.Items.Clear();
                    foreach (var item in Pacote.GetLista())
                    {

                          ComboBox.Items.AddRange(
                          new object[] { item.ToString() });
                    }
                    MessageBox.Show("O pacote foi eliminado");
                }
            }

        }
    }
}
